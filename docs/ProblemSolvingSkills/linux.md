# Linux常用命令，生产环境服务器变慢诊断，线上排查，性能评估？

    查看整机：top
    查看CPU：vmstat
    查看内存：free
    查看硬盘：df
    查看磁盘IO：lostat -xdk 间隔秒数 次数
    查看网络IO：ifstat



CPU占用过高定位分析思路

    使用top命令找出cpu占比最高的
    使用ps -ef或者jps进一步定位，得知是怎样的一个后台程序出问题了
    使用ps -mp pid（线程id） -o THREAD,tid,time 定位到具体的线程或者代码
    使用printf “%x\n” tid 把线程ID转换为16进制格式
    使用jstack pid |grep tid -A 30打印线程的堆栈信息，定位具体的行数