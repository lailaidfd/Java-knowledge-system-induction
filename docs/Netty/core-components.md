# 核心组件？

一、Channel:

1、客户端连接上服务器之后,就会产生一个channel,就是一个通道。
2、可以理解为socket,通过channel来完成IO操作(read、write、bind、connect等等)，也是数据入站和出站的载体。

二、EventLoop 和 EventLoopGroup

1、EventLoop就是一个用于监控事件的一个组件,所有Channel都会被分配到具体的EventLoop上。
2、一个EventLoop可以监控多个channel。
3、EventLoop本质上就是一个线程,不断的轮询,监控多个channel,看IO操作的数据是否准备就绪,如果就绪就调用相应的操作。
4、EventLoopGroup就是一个线程池,包含了多个 EventLoop 对象。
5、EventLoopGroup 做的事情就是创建一个新的 Channel，并且给它分配一个 EventLoop。
6、EventLoopGroup，EventLoop 和 Channel 的关系
 <1>、客户端发起连接请求,EventLoopGroup负载生成Channel,将这个Channel注册到一个EventLoop上。
 <2>、EventLoop实际上就是一个线程,去循环监听多个Channel的IO事件,监听到了就执行对应的回调函数。
 <3>、在数据返回客户端之前,客户端的用户线程是不会阻塞的,可以继续执行其它的操作。
 <4>、当检测到服务器端的数据到达客户端系统内存时,再去另外开一条线程处理,将系统内存复制到用户内存。(异步阻塞)
 <5>、整个过程 EventLoop 就是监视器+传声筒,具体的业务利用线程池去执行。

三、ChannelHandler，ChannelPipeline 和 ChannelHandlerContext

1、如果说 EventLoop 是事件的通知者，那么 ChannelHandler 就是事件的处理者。

2、ChannelHandler 就是具体处理业务的组件,针对出站和入站的事件，有不同的 ChannelHandler，分别是:
 .ChannelInBoundHandler(入站事件处理器)
 .ChannelOutBoundHandler(出站事件处理器)
 
3、ChannelPipeline:
<1>、ChannelPipeline是一个管道,里面包含了多个阀门(ChannelHandler ),消息通过指定顺序执行多个ChannelHandler 。
<2>、ChannelPipeline 负责“排队”(处理事件的顺序),入站(服务接收消息)是顺序,出站(服务推送消息)是逆序。
<3>、ChannelPipeline 可以添加或者删除 ChannelHandler，管理整个队列。
<4>、ChannelPipeline 来管理多个不同的ChannelHandler之间的执行顺序,ChannelHandlerContext 负责传递消息,在每个ChannelHandler 中处理事件方法的，形参里面都有ChannelHandlerContext这个对象。

### Bootstrap 和 ServerBootstrap 
Bootstrap 通常使用 connet() 方法连接到远程的主机和端口，作为一个 Netty TCP 协议通信中的客户端。另外，Bootstrap 也可以通过 bind() 方法绑定本地的一个端口，作为 UDP 协议通信中的一端。ServerBootstrap通常使用 bind() 方法绑定本地的端口上，然后等待客户端的连接。Bootstrap 只需要配置一个线程组— EventLoopGroup ,而 ServerBootstrap需要配置两个线程组— EventLoopGroup ，一个用于接收连接，一个用于具体的处理。

### NioEventLoopGroup 默认的构造函数会起多少线程呢？
NioEventLoopGroup 默认的构造函数实际会起的线程数为 CPU核心数*2。每个NioEventLoopGroup对象内部都会分配一组NioEventLoop，其大小是 nThreads, 这样就构成了一个线程池， 一个NIOEventLoop和一个线程相对应。





