# 集群部署时的分布式 Session 如何实现？

1。tomcat + redis
2。spring session + redis 


（1）tomcat + redis
使用session的代码跟以前一样，还是基于tomcat原生的session支持即可，然后就是用一个叫做Tomcat RedisSessionManager的东西，在tomcat的配置文件中，配置一下,让所有我们部署的tomcat都将session数据存储到redis即可。

（2）spring session + redis 
给sping session配置基于redis来存储session数据，然后配置了一个spring session的过滤器，这样的话，session相关操作都会交给spring session来管了。接着在代码中，就用原生的session操作，就是直接基于spring sesion从redis中获取数据了。
