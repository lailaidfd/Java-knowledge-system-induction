# 堆溢出，栈溢出，方法区溢出？

堆溢出：

    package kan;
    /**
     * VM Args : -Xms1m -Xmx1m
     */
     
    import java.util.ArrayList;
    import java.util.List;
     
    public class Overheap {
     
    	static class OOheap {
     
    	}
     
    	public static void main(String[] args) {
     
    		List<OOheap> l = new ArrayList<OOheap>();
    		while (true) {
    			l.add(new OOheap());
    		}
    	}
    }

栈溢出：

    /**
     * VM Args : -Xss100k
     * 
     * 
     */
    public class OverStack {
     
    	public void leakStack() {
    		leakStack();
    	}
     
    	public static void main(String[] args) {
    		OverStack o = new OverStack();
    		try {
    			o.leakStack();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    }

方法区溢出：（偷懒溢出常量池）

    /**
     * VM Args: -XX:PremSize=100k -XX:MaxPremSize=100k
     */
    import java.util.ArrayList;
    import java.util.List;
     
    public class OverPremGen {
     
    	public static void main(String[] args) {
     
    		List l = new ArrayList();
    		int i = 0;
    		while(true){
    			
    			l.add(String.valueOf(i++).intern());
    		}
    	}
    }


JVM 堆内存溢出后，其他线程是否可继续工作？

    当一个线程抛出OOM异常后，它所占据的内存资源会全部被释放掉，使用堆的数量，突然间急剧下滑，从而不会影响其他线程的运行！其实发生OOM的线程一般情况下会死亡，也就是会被终结掉，该线程持有的对象占用的heap都会被gc了，释放内存。因为发生OOM之前要进行gc，就算其他线程能够正常工作，也会因为频繁gc产生较大的影响。

溢出和泄漏：

        内存溢出：程序在申请内存的时候，没有足够的内存可以分配，导致内存溢出。俗称，内存不够了。
        内存泄漏：内存在生命周期完成后，如果得不到及时的释放，就会一直占用内存，造成内存泄漏。随着内存泄漏的堆积，可用的内存空间越来越少，最后会导致内存溢出。
