# 数据类型的应用场景？

## 字符串string

可以通过set key value 实现单值缓存

可以通过setnx product:10001 true 实现分布式锁，返回1表示获取锁成功，返回0表示获取锁失败，这个值已经被设置过

可以通过incr acticle:readcount{文章id} 实现计数器，每执行一次加一

可以通过incrby orderId 1000 实现分布式系统全局序列号，一次性拿1000个序列号，在redis里面加一，批量生成序列号提升性能

因为string 类型是二进制安全的，可以用来存放图片，视频等内容，另外由于Redis的高性能读写功能，而string类型的value也可以是数字，可以用作计数器（INCR,DECR），比如分布式环境中统计系统的在线人数，秒杀等。


## 哈希hash
哈希hash（比string类型操作消耗内存和cpu更小，更节约空间，集群架构下不适合大规模使用）

可以通过hmget user 1:name 1:balance 实现对象缓存：多个字段修改方便一些，大数据量要进行分段存储

电商购物车实现场景可以通过hset 添加商品 hincrby 添加商品数量 hlen获取商品总数 hdel删除商品 hgetall获取购物车所有商品

可以做单点登录存放用户信息

## 列表list

lpush把值设置到列表的表头（最左边）rpush把值设置到列表的表尾（最右边）lpop从列表头拿掉值（最左边）rpop从列表尾拿掉值（最右边）

Stach(栈)：lpush+lpop

Queue(队列)：lpush+rpop

Blocking MQ(阻塞队列)：lpush + brpop

微博消息和微信公众号文章，我关注了MacTalk和备胎说车，MacTalk先发了一篇文章，文章id为10018，备胎说车后发了一篇文章，文章id为10086

查看最新消息，LRANGE msg:{我的用户id} 0 4//查看自己订阅文章的最新五篇

可以实现简单的消息队列，另外可以利用lrange命令，做基于redis的分页功能

## 集合set

微信抽奖小程序

sadd key {userId} 点击参与抽奖加入集合

smembers key 查看参与抽奖所有用户

srandmember key [count] /spop key [count] 抽取count名中奖者

微信微博点赞，收藏，标签

sadd key value 点赞

srem key value 取消点赞

sismember key value 检查用户是否点过赞

smembers key 获取点赞的用户列表

scard key 获取点赞用户数

实现微博微信关注模型

SINTER 交集 取共同的元素 可以实现商品筛选面包屑

SUNION 并集 取所有的元素

SDIFF 差集 以第一个集合为基准减去后面所有集合的并集，最后看第一个集合还剩下的元素

共同关注的人：交集

我可能认识的人：进到王五的主页,拿王五的集合和自己的集合对比，取差集

我关注的人也关注他：取出自己关注人的集合的交集

由于底层是字典实现的，查找元素特别快，另外set 数据类型不允许重复，利用这两个特性我们可以进行全局去重，比如在用户注册模块，判断用户名是否注册；另外就是利用交集、并集、差集等操作，可以计算共同喜好，全部的喜好，自己独有的喜好等功能。


## zset

实现排行榜

zincrby key count 实现点击新闻

zreverange key 0 9 withscores 实现展示当日排行前十

zunionstore key count 几日搜索榜单

zreveange key 0 9 withscores 展示七日排行前十

有序的集合，可以做范围查找，排行榜应用，取 TOP N 操作等。
