# 自旋锁?

自旋锁：获取锁的线程不会立即堵塞，使用循环的方式去尝试获取锁，减少线程上下文切换的消耗，会消耗CPU资源。