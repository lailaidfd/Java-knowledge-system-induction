# 读写锁？

独占锁（写锁）：锁一次只能被一个线程所持有，ReentrantLock和Synchronized都是独占锁。
共享锁（读锁）：锁可以被多个线程持有，可以保证并发的读。ReentrantReadWriteLock的读锁是共享锁，写锁是独占锁。
读写锁：使用ReentrantReadWriteLock解决原子性和独占性，可以很好的解决并发性和数据的一致性。起因：一个线程去写共享资源，其他线程就不能对它进行读写。写的操作原子性和独占性没有得到保证，一个线程正在写入共享资源的时候，其他线程有写入和读取的共享资源操作，导致数据不一致。writeLock和readLock方法都是通过调用Sync方法实现的。AQS的状态state（int类型）是32的，掰成二瓣，读锁使用高16位表示读锁的线程数，写锁使用低16位表示写锁的重入次数。状态值位0表示锁空闲，读状态为2，写状态为3，sharedCount不为0表示分配了读锁，exclusiveCount不为0表示分配了写锁。

private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

写锁：

    lock.writeLock().lock();//加上写锁		
    try {
    	//执行写操作
    } catch (InterruptedException e) {
    	e.printStackTrace();
    }finally {
    	lock.writeLock().unlock();//释放锁
    }

读锁：

    lock.readLock().lock();//加上读锁
    try {
    	//执行读操作
    } catch (InterruptedException e) {
    	e.printStackTrace();
    }finally {
    	lock.readLock().unlock();//释放读锁
    }

