# 公平锁，非公平锁？

公平锁：多个线程按照申请锁的顺序来获取锁的，类似排队打饭，先来后到。每个线程在获取锁的时候会先查看此锁维护的等待队列，如果为空或者当前线程是等待队列的第一个，就占有锁，否则加入到等待队列之后，后面按照规则从队列中获取。

非公平锁：多个线程获取锁的顺序并不是按照申请锁的顺序来到，高并发情况下，后申请的线程可能比先申请的线程优先获取锁。非公平锁上来就直接尝试占有锁，如果尝试失败，在使用公平锁的方式。它的吞吐量比公平锁大。ReentrantLock默认是非公平锁，Synchronized就是非公平锁。
