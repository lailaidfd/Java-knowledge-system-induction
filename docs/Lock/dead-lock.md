# 死锁？

产生死锁的四个必要条件

        互斥条件：进程要求对所分配的资源（如打印机）进行排他性控制，即在一段时间内某资源仅为一个进程所占有。此时若有其他进程请求该资源，则请求进程只能等待。
        不可剥夺条件:进程所获得的资源在未使用完毕之前，不能被其他进程强行夺走，即只能由获得该资源的进程自己来释放（只能是主动释放)。
        请求与保持条件：进程已经保持了至少一个资源，但又提出了新的资源请求，而该资源已被其他进程占有，此时请求进程被阻塞，但对自己已获得的资源保持不放。
        循环等待条件:存在一种进程资源的循环等待链，链中每一个进程已获得的资源同时被 链中下一个进程所请求。即存在一个处于等待状态的进程集合{Pl, P2, …, pn}，其中Pi等 待的资源被P(i+1)占有（i=0, 1, …, n-1)，Pn等待的资源被P0占有

处理死锁的方法

            预防死锁：通过设置某些限制条件，去破坏产生死锁的四个必要条件中的一个或几个条件，来防止死锁的发生。
            避免死锁：在资源的动态分配过程中，用某种方法去防止系统进入不安全状态，从而避免死锁的发生。
            检测死锁：允许系统在运行过程中发生死锁，但可设置检测机构及时检测死锁的发生，并采取适当措施加以清除。
            解除死锁：当检测出死锁后，便采取适当措施将进程从死锁状态中解脱出来。

避免死锁的技术

            加锁顺序（线程按照一定的顺序加锁）
            加锁时限（线程尝试获取锁的时候加上一定的时限，超过时限则放弃对该锁的请求，并释放自己占有的锁）
            死锁检测 （首先为每一个进程和每一个资源指定一个唯一的号码；而后创建资源分配表和进程等待表）

    死锁检测工具：

        Jstack命令：用于打印出给定的java进程ID或core file或远程调试服务的Java堆栈信息，生成java虚拟机当前时刻的线程快照。生成线程快照的主要目的是定位线程出现长时间停顿的缘由，如线程间死锁、死循环、请求外部资源致使的长时间等待等。 线程出现停顿的时候经过jstack来查看各个线程的调用堆栈，就能够知道没有响应的线程到底在后台作什么事情，或者等待什么资源。
        JConsole工具：用于链接正在运行的本地或者远程的JVM，对运行在Java应用程序的资源消耗和性能进行监控，并画出大量的图表，提供强大的可视化界面。

什么是死锁？锁等待？如何优化这类问题？通过数据库哪些表可以监控？

    死锁是指两个或多个事务在同一资源上互相占用，并请求加锁时，而导致的恶性循环现象。当多个事务以不同顺序试图加锁同一资源时，就会产生死锁。

锁等待：mysql数据库中，不同session在更新同行数据中，会出现锁等待

    重要的三张锁的监控表innodb_trx，innodb_locks，innodb_lock_waits

 如何优化锁：

    1、尽可能让所有的数据检索都通过索引来完成，从而避免Innodb因为无法通过索引键加锁而升级为表级锁定

    2、合理设计索引。不经常使用的列最好不加锁

    3、尽可能减少基于范围的数据检索过滤条件
