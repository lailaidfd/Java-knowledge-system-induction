# Dubbo 的工作原理？注册中心挂了可以继续通信吗？

第一步：provider 向注册中心去注册
第二步：consumer 从注册中心订阅服务，注册中心会通知 consumer 注册好的服务
第三步：consumer 调用 provider
第四步：consumer 和 provider 都异步通知监控中心

服务注册与发现
1、Provider(提供者)绑定指定端口并启动服务
2、供者连接注册中心，并发本机 IP、端口、应用信息和提供服务信息发送至注册中心存储
3、Consumer(消费者），连接注册中心 ，并发送应用信息、所求服务信息至注册中心
4、注册中心根据消费者所求服务信息匹配对应的提供者列表发送至Consumer 应用缓存。
5、Consumer 在发起远程调用时基于缓存的消费者列表择其一发起调用。
6、Provider 状态变更会实时通知注册中心、在由注册中心实时推送至Consumer设计的原因：Consumer 与 Provider 解偶，双方都可以横向增减节点数。注册中心对本身可做对等集群，可动态增减节点，并且任意一台宕掉后，将自动切换到另一台
7、去中心化，双方不直接依懒注册中心，即使注册中心全部宕机短时间内也不会影响服务的调用
8、服务提供者无状态，任意一台宕掉后，不影响使用


注册中心挂了可以继续通信吗？
可以，因为刚开始初始化的时候，消费者会将提供者的地址等信息拉取到本地缓存，所以注册中心挂了可以继续通信。
