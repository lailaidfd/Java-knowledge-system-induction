# 主从问题？

## 主从常见问题
主从备份不可靠，需要人经常去查看同步状态，一旦出现报错，需要及时人为的处理。
出现1236：[ERROR] Slave I/O: Got fatal error 1236 from master when reading data from binary log: 'Client requested master to start replication from impossible position', Error_code: 1236
解决方案：主从服务器失去连接，出现了宕机的情况。常用解决办法，重新查询主服务器的状态，获取新的position位置，重新设置从服务器的同步信息。设置命令为：change master to master_log_file='',master_log_pos=123;

出现1032：Last_Errno: 1032， Last_Error: Could not execute Update_rows event on table xuanzhi.test; Can't find record in 'test', Error_code: 1032; handler error HA_ERR_KEY_NOT_FOUND; the event's master log mysql
解决方案：从数据库上面缺少某一条数据记录，主数据库对这条记录又做了修改，从数据库在修改时报错。解决方案是直接用数据库管理工具，数据传输模式处理具体异常的数据表，保证主数据与从数据库对应的报错数据表结构信息一样。

出现1062：Last_Errno: 1062，Last_Error: Could not execute Write_rows event on table xuanzhi.test; Duplicate entry '5' for key 'PRIMARY', Error_code: 1062; handler error HA_ERR_FOUND_DUPP_KEY; the event's master log 
解决方案：出现1062，表示主键冲突，及从数据库上面出现了主数据库上面没有的主键信息记录。解决方案是直接删除提示的从数据库中的异常数据，或者利用数据传输模式处理具体异常的数据表。

中继日志错误：Last_Errno: 1594，Last_Errno: 1593
解决方案：一般是服务器宕机引起，解决方案和出现错误1236一样。在mysql 5.5以上版本，可在slave的配置文件my.cnf里要增加一个参数relay_log_recovery=1。

mysql主从复制，经常会遇到错误而导致slave端复制中断，这个时候一般就需要人工干预，跳过错误才能继续。跳过错误有两种方式：
1.跳过指定数量的事务: SET GLOBAL SQL_SLAVE_SKIP_COUNTER = 1 #跳过一个事务
2.修改mysql的配置文件，通过slave_skip_errors参数来跳所有错误或指定类型的错误：slave-skip-errors=1062,1053,1146 #跳过指定error no类型的错误。slave-skip-errors=all #跳过所有错误
校验主从服务器上面的数据是否完全一致，可通过工具pt-table-checksum操作。
 
