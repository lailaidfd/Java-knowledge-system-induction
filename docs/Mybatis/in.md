# SQL中使用in在Mybatis框架中有什么标签可以使用？


## 如果传的参数为list 的值 例如: [2,2,2,2,2,2,2,2]


#### 使用 foreach标签

foreach元素的属性主要有 item，index，collection，open，separator，close。

item表示集合中每一个元素进行迭代时的别名.

index指 定一个名字，用于表示在迭代过程中，每次迭代到的位置.

open表示该语句以什么开始，separator表示在每次进行迭代之间以什么符号作为分隔 符.

close表示以什么结束.

到.xml 中的传值是WHERE ID IN
<foreach item="idItem" collection="delIds" open="(" separator="," close=")"> #{idItem}



##如果传的参数为 Map 传值 例如: params:3,3,3,3,3,3

到xml中的传值是

#### ${}占位符
WHERE ID IN (${idsl})



