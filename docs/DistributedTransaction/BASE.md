# BASE理论？

BASE 理论是对 CAP 理论的延伸，核心思想是即使无法做到强一致性，但应用可以采用适合的方式达到最终一致性。
基本可用： 基本可用是指分布式系统在出现故障的时候，允许损失部分可用性，即保证核心可用。电商大促时，为了应对访问量激增，部分用户可能会被引导到降级页面，服务层也可能只提供降级服务。这就是损失部分可用性的体现。
软状态： 软状态是指允许系统存在中间状态，而该中间状态不会影响系统整体可用性。分布式存储中一般一份数据至少会有三个副本，允许不同节点间副本同步的延时就是软状态的体现。MySQL Replication 的异步复制也是一种体现。
最终一致性： 最终一致性是指系统中的所有数据副本经过一定时间后，最终能够达到一致的状态。弱一致性和强一致性相反，最终一致性是弱一致性的一种特殊情况。
         
应用场景：
Erueka:erueka是SpringCloud系列用来做服务注册和发现的组件，作为服务发现的一个实现，在设计的时候就更考虑了可用性，保证了AP。
Zookeeper:Zookeeper在实现上牺牲了可用性，保证了一致性（单调一致性）和分区容错性，也即：CP。所以这也是SpringCloud抛弃了zookeeper而选择Erueka的原因。

具体根据各自业务场景所需来制定相应的策略而选择适合的产品服务等。例如：支付订单场景中，由于分布式本身就在数据一致性上面很难保证，从A服务到B服务的订单数据有可能由于服务宕机或其他原因而造成数据不一致性。因此此类场景会酌情考虑：AP，不强制保证数据一致性，但保证数据最终一致性。
分布式事务指事务的操作位于不同的节点上，需要保证事务的 AICD 特性，在下单场景下，库存和订单如果不在同一个节点上，就涉及分布式事务。
