# Spring IOC，Spring AOP？

AOP：面向切面编程。

即在一个功能模块中新增其他功能，比方说你要下楼取个快递，你同事对你说帮我也取一下呗，你就顺道取了。在工作中如果系统中有些包和类中没有使用AOP，例如日志，事务和异常处理，那么就必须在每个类和方法中去实现它们。 代码纠缠每个类和方法中都包含日志，事务以及异常处理甚至是业务逻辑。在一个这样的方法中，很难分清代码中实际做的是什么处理。AOP 所做的就是将所有散落各处的事务代码集中到一个事务切面中。

场景

比方说我现在要弄一个日志，记录某些个接口调用的方法时间。使用Aop我可以在这个接口前插入一段代码去记录开始时间，在这个接口后面去插入一段代码记录结束时间。

又或者你去访问数据库，而你不想管事务（太烦），所以，Spring在你访问数据库之前，自动帮你开启事务，当你访问数据库结束之后，自动帮你提交/回滚事务！

异常处理你可以开启环绕通知，一旦运行接口报错，环绕通知捕获异常跳转异常处理页面。

动态代理

Spring AOP使用的动态代理，所谓的动态代理就是说AOP框架不会去修改字节码，而是在内存中临时为方法生成一个AOP对象，这个AOP对象包含了目标对象的全部方法，并且在特定的切点做了增强处理，并回调原对象的方法。它的动态代理主要有两种方式，JDK动态代理和CGLIB动态代理。JDK动态代理通过反射来接收被代理的类，并且要求被代理的类必须实现一个接口。JDK动态代理的核心是InvocationHandler接口和Proxy类。如果目标类没有实现接口，那么Spring AOP会选择使用CGLIB来动态代理目标类。CGLIB是一个代码生成的类库，可以在运行时动态的生成某个类的子类，注意，CGLIB是通过继承的方式做的动态代理，因此如果某个类被标记为final，那么它是无法使用CGLIB做动态代理的。

IOC：依赖注入或者叫做控制反转。

正常情况下我们使用一个对象时都是需要new Object()的。而ioc是把需要使用的对象提前创建好，放到spring的容器里面。

所有需要使用的类都会在spring容器中登记，告诉spring你是个什么东西，你需要什么东西，然后spring会在系统运行到适当的时候，把你要的东西主动给你，同时也把你交给其他需要你的东西。所有的类的创建、销毁都由 spring来控制，也就是说控制对象生存周期的不再是引用它的对象，而是spring。DI(依赖注入)其实就是IOC的一种实现方式。

场景：

正常情况下我们使用一个对象时都是需要new Object() 的。而ioc是把需要使用的对象提前创建好，放到spring的容器里面。需要使用的时候直接使用就行，而且可以设置单例或多例，非常灵活。

我们在service层想调用另外一个service的方法，不需要去new了，直接把它交给spring管理，然后用注解的方式引入就能使用。

IOC三种注入方式

（1）XML：Bean实现类来自第三方类库，例如DataSource等。需要命名空间等配置，例如：context，aop，mvc。

（2）注解：在开发的类使用@Controller，@Service等注解

（3）Java配置类：通过代码控制对象创建逻辑的场景。例如：自定义修改依赖类库。


