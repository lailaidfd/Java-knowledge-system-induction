# Spring Cloud 和 Dubbo 有哪些区别？

dubbo 是二进制传输，占用带宽会少一点。SpringCloud是http 传输，带宽会多一点，同时使用http协议一般会使用JSON报文，消耗会更大。
dubbo 开发难度较大，所依赖的 jar 包有很多问题大型工程无法解决。SpringCloud 对第三方的继承可以一键式生成，天然集成。
SpringCloud 接口协议约定比较松散，需要强有力的行政措施来限制接口无序升级。
最大的区别:
Spring Cloud抛弃了Dubbo 的RPC通信，采用的是基于HTTP的REST方式。

SpringCloud是Apache旗下的Spring体系下的微服务解决方案。
Dubbo是阿里系的分布式服务治理框架。
从技术维度上,其实SpringCloud远远的超过Dubbo,Dubbo本身只是实现了服务治理。