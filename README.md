# 互联网 Java 工程师知识扫盲

## 个人介绍
我是廖志伟，一名java开发工程师，CSDN博客专家，多年一线研发经验，曾就职多家互联网公司，任Java开发工程师职位，参与多个千万级并发互联网产品研发，对大型分布式，高并发及微服务架构有非常深入研究。

## 项目说明
本项目大部分内容来自廖志伟编写，版权归作者所有，内容涵盖[Java基础](#JavaSE)、[框架](#Spring,#Springboot,#SpringCould,#Dubbo,#Zookeeper,#Mybatis)、[设计模式](#设计模式)、[缓存](#Redis)、[多线程](#线程池,#JUC)、[分布式事务](#两阶段提交（2PC）,#三阶段提交(3PC),#补偿事务（TCC）,#本地消息表（异步确保）,#MQ事务消息)、[数据库](#索引的数据结构,集群问题,同步问题,Sql优化)、[JVM](#算法,#垃圾回收器,#内存模型,#双亲委派)、[锁](#公平锁，#非公平锁，#可重入锁，#递归锁，#自旋锁，#读写锁，#悲观锁，#乐观锁，#行锁，#排它锁，#共享锁，#表锁，死锁，#分布式锁，#AQS，#Synchronized)、[高并发](#高并发架构)、[分布式](#分布式系统)、[高可用](#高可用架构)、[微服务](#微服务架构)、[海量数据处理](#海量数据处理)等领域知识。我对这部分知识做了一个系统的整理，方便学习查阅。

## 内容模块
1.  Java基础
2.  框架
3.  设计模式
4.  缓存
5.  多线程
6.  分布式事务
7.  数据库
8.  JVM
9.  锁
10. 消息中间件
11. 搜索引擎
12. 海量数据处理
13. 解决问题的能力
14. 网络
15. Netty
16. 功能模块设计
17. 日志
18. 高并发

## Java基础

### [HashMap](./docs/HashMap)

- [HashMap底层原理？](./docs/HashMap/underlying-implementation-principles.md)
- [HashMap扩容机制？](./docs/HashMap/capacity-expansion-mechanism.md)
- [HashMap加载因子为什么是0.75？](./docs/HashMap/loading-factor.md)
- [HashMap为什么会发生并发修改异常？并发修改异常解决方案？](./docs/HashMap/concurrent-modification-exception.md)
- [为什么jdk8以后HashMap会使用红黑树优化？](./docs/HashMap/mangrove-black.md)
- [CAS，ABA，volatile这些是什么？](./docs/HashMap/CAS-ABA-volatile.md)

## 框架

### [Spring](./docs/Spring)

- [Spring IOC，Spring AOP？](./docs/Spring/ioc-aop.md)
- [Springbean的生命周期？](./docs/Spring/life-cycle.md)
- [如何解决循环依赖？](./docs/Spring/circular-dependence.md)
- [父子容器？](./docs/Spring/parent-child-container.md)
- [事务实现原理？](./docs/Spring/transaction-implementation-principle.md)
- [动态代理原理？](./docs/Spring/principle-of-dynamic-agent.md)
- [Spring MVC工作原理？](./docs/Spring/spring-MVC-works.md)
- [Spring如何实现事务？](./docs/Spring/implement-transaction-spring.md)

### [Mybatis](./docs/Mybatis)

- [Mybatis框架优点？](./docs/Mybatis/mybatis-frame.md)
- [SQL中使用in在Mybatis框架中有什么标签可以使用？](./docs/Mybatis/in.md)
- [mybatis分页实现有几种实现方式？](./docs/Mybatis/limit.md)

### [Dubbo](./docs/Dubbo)

- [说一下 Dubbo 的工作原理？注册中心挂了可以继续通信吗？](./docs/Dubbo/working-principle.md)
- [Dubbo 支持哪些序列化协议？说一下 Hessian 的数据结构？PB 知道吗？为什么 PB 的效率是最高的？](./docs/Dubbo/agreement.md)
- [Dubbo 负载均衡策略和集群容错策略都有哪些？动态代理策略呢？](./ddocs/Dubbo/strategy.md)
- [为什么需要服务治理？如何基于 Dubbo 进行服务治理、服务降级、失败重试以及超时重试？](./docs/Dubbo/service-governance.md)
- [分布式服务接口的幂等性如何设计（比如不能重复扣款）？](./docs/Dubbo/Idempotency.md)
- [分布式服务接口请求的顺序性如何保证？](./docs/Dubbo/sequence.md)
- [如何自己设计一个类似 Dubbo 的 RPC 框架？](./docs/Dubbo/RPC.md)
- [Dubbo特性？](./docs/Dubbo/characteristic.md)
- [Dubbo SPI 和 Java SPI 区别？](./docs/Dubbo/spi.md)
- [Dubbo 用到哪些设计模式？](./docs/Dubbo/design-pattern.md)
- [为什么要进行系统拆分？如何进行系统拆分？拆分后不用 Dubbo 可以吗？](./docs/Dubbo/system-split.md)

### [Zookeeper](./docs/Zookeeper)

- [Zookeeper底层原理？](./docs/Zookeeper/underlying-principle.md)
- [选举机制？](./docs/Zookeeper/election-mechanism.md)
- [Zookeeper脑裂问题？](./docs/Zookeeper/cerebral-fissure.md)
- [Zookeeper假死问题？](./docs/Zookeeper/feign-death.md)
- [Zookeeper分布式锁？](./docs/Zookeeper/distributed-lock.md)
- [Zookeeper客户端与集群问题？](./docs/Zookeeper/client-cluster-problems.md)
- [ZooKeeper典型使用场景？](./docs/Zookeeper/typical-use-scenarios.md)

### [SpringCould](./docs/SpringCould)

- [什么是微服务？](./docs/SpringCould/microservice.md)
- [常用组件底层实现？](./docs/SpringCould/assembly.md)
- [服务发现组件 Eureka 的主要调用过程？Eureka 和 Zookeeper 都可以提供服务注册与发现的功能，它们有什么区别？](./docs/SpringCould/service-registration-discovery.md)
- [熔断框架如何做技术选型？选用 Sentinel 还是 Hystrix？](./docs/SpringCould/fuse-degradation.md)
- [如何限流？在工作中是怎么做的？说一下具体的实现？](./docs/SpringCould/current-limiting.md)
- [Spring Boot 和 Spring Cloud，谈谈你对它们的理解？](./docs/SpringCould/boot-difference-could.md)
- [Spring Cloud 和 Dubbo 有哪些区别？](./docs/SpringCould/dubbo-difference-could.md)

## 设计模式

### [设计模式](./docs/DesignModel)

- [23种设计模式有哪些？](./docs/DesignModel/23design-pattern.md)
- [讲讲你熟悉的几个设计模式底层实现原理？](./docs/DesignModel/design-pattern.md)

## 缓存

### [Redis](./docs/Redis)

- [你是如何理解redis单线程模型的？](./docs/Redis/single-thread-model.md)
- [Redis 的持久化有哪几种方式？不同的持久化机制都有什么优缺点？持久化机制具体底层是如何实现的？save与bgsave？](./docs/Redis/persistence.md)
- [Redis 集群模式的工作原理能说一下么？集群元数据的维护：集中式、Gossip 协议？在集群模式下，Redis 的 key 是如何寻址的？分布式寻址都有哪些算法？了解一致性 hash 算法吗？如何动态增加和删除一个节点？Redis cluster 的高可用与主备切换原理？](./docs/Redis/cluster-mode.md)
- [Redis 的并发竞争问题是什么？如何解决这个问题？了解 Redis 事务的 CAS 方案吗？](./docs/Redis/concurrent-competition.md)
- [生产环境中的 Redis 是怎么部署的？](./docs/Redis/deploy.md)
- [数据类型的应用场景？](./docs/Redis/application-scenarios.md)
- [数据类型的底层编码？](./docs/Redis/data-structure.md)
- [布隆过滤器？](./docs/Redis/bloom-filter.md)
- [数据同步问题（双删策略），数据一致性？](./docs/Redis/data-synchronization.md)
- [击穿、穿透、雪崩？](./docs/Redis/breakdown-penetration-avalanche-consistency.md)
- [高并发下的redis分布式锁？](./docs/Redis/distributed-lock.md)
- [热点数据缓存问题？](./docs/Redis/hot-data.md)
- [哨兵机制？](./docs/Redis/sentry-mechanism-cluster-architecture.md)
- [生产环境常见性能问题及问题分析过程？](./docs/Redis/production-environment.md)
- [Redis 的过期策略都有哪些？](./docs/Redis/expiration-policies.md)
- [如何保证 Redis 高并发、高可用？Redis 的主从复制原理能介绍一下么？Redis 的哨兵原理能介绍一下么？主从架构下的数据部分复制？](./docs/Redis/master-slave-replication.md)

## 多线程

### [多线程](./docs/ThreadPool)

- [常见的四种线程池和区别？](./docs/ThreadPool/threadPool-difference.md)
- [线程池实现原理？](./docs/ThreadPool/implementation-principle.md)
- [线程池七大核心参数？](./docs/ThreadPool/core-parameters.md)
- [线程池如何合理的配置核心线程数？](./docs/ThreadPool/configure.md)
- [线程池拒绝策略？](./docs/ThreadPool/rejection-strategy.md)
- [JUC并发包？](./docs/ThreadPool/juc.md)
- [ThreadLocal 是什么？ThreadLocal 工作原理是什么？ThreadLocal 如何解决 Hash 冲突？ThreadLocal 的内存泄露是怎么回事？为什么 ThreadLocalMap 的 key 是弱引用？ThreadLocal 的应用场景有哪些？](./docs/ThreadPool/thread-pool.md)
- [线程的生命周期，什么时候会出现孤儿进程，僵尸进程？它们之间的危害是什么？如何处理僵尸进程？](./docs/ThreadPool/life-cycle.md)

## 分布式事务

### [分布式事务](./docs/DistributedTransaction)

- [CAP理论？](./docs/DistributedTransaction/CAP.md)
- [BASE理论？](./docs/DistributedTransaction/BASE.md)
- [两阶段提交（2PC）？](./docs/DistributedTransaction/2pc.md)
- [三阶段提交（3PC）？](./docs/DistributedTransaction/3pc.md)
- [补偿事务（TCC）？](./docs/DistributedTransaction/tcc.md)
- [本地消息表（异步确保）？](./docs/DistributedTransaction/local-message-table.md)
- [MQ 事务消息？](./docs/DistributedTransaction/MQ-transaction-message.md)
- [最大努力通知？](./docs/DistributedTransaction/best-effort-notification.md)
- [可靠消息最终一致性方案?](./docs/DistributedTransaction/reliable-message-final-consistency-scheme.md)

### [分布式会话](./docs/DistributedSession)

- [集群部署时的分布式 Session 如何实现？](./docs/DistributedSession/realization.md)

## 数据库

### [数据库](./docs/MySQL)

- [数据库事务隔离级别？](./docs/MySQL/Isolation-level.md)
- [四大属性底层实现原理？](./docs/MySQL/four-attributes.md)
- [传播行为？](./docs/MySQL/communication-behavior.md)
- [悲观锁、乐观锁、排它锁、共享锁、表级锁、行级锁，死锁？](./docs/MySQL/lock.md)
- [索引数据结构？](./docs/MySQL/Index-data-structure.md)
- [主从问题？](./docs/MySQL/master-slave-problem.md)
- [SQL的整个解析、执行过程原理、SQL行转列？](./docs/MySQL/SQL.md)
- [如何优化SQL？](./docs/MySQL/optimization.md)
- [vachar和char的区别？](./docs/MySQL/optimization.md)

## [读写分离](./docs/MySQL/read-write-separation.md)

- [如何实现 MySQL 的读写分离？MySQL 主从复制原理是啥？如何解决 MySQL 主从同步的延时问题？](./docs/MySQL/read-write-separation.md)

### [分库分表](./docs/MySQL/sub-database-sub-table.md)

- [为什么要分库分表（设计高并发系统的时候，数据库层面该如何设计）？用过哪些分库分表中间件？不同的分库分表中间件都有什么优点和缺点？你们具体是如何对数据库如何进行垂直拆分或水平拆分的？](./docs/MySQL/sub-database-sub-table.md)
- [现在有一个未分库分表的系统，未来要分库分表，如何设计才可以让系统从未分库分表动态切换到分库分表上？](./docs/MySQL/design.md)
- [如何设计可以动态扩容缩容的分库分表方案？](./docs/MySQL/dynamic-expansion.md)
- [分库分表之后，id 主键如何处理？](./docs/MySQL/primary-key.md)

## JVM

### [JVM](./docs/JVM)

- [JVM算法？](./docs/JVM/algorithm.md)
- [垃圾收集器？](./docs/JVM/garbage-collector.md)
- [垃圾回收机制？](./docs/JVM/recycling-mechanism.md)
- [JMM和JVM内存模型？](./docs/JVM/memory-model.md)
- [JVM调优？](./docs/JVM/JVM-tuning.md)
- [双亲委派机制？](./docs/JVM/parental-appointment-mechanism.md)
- [堆溢出，栈溢出，方法区溢出？](./docs/JVM/Heap-stack-method-overflow.md)
- [你都有哪些手段用来排查内存溢出？](./docs/JVM/Investigation.md)
- [各种oom的种类？](./docs/JVM/type.md)

## 锁

### [锁](./docs/Lock/)

- [公平锁，非公平锁？](./docs/Lock/fair-lock.md)
- [可重入锁，递归锁？](./docs/Lock/reentrant-lock.md)
- [自旋锁？](./docs/Lock/spin-lock.md)
- [读写锁？](./docs/Lock/read-write-lock.md)
- [死锁？](./docs/Lock/dead-lock.md)
- [分布式锁？](./docs/Lock/distributed-lock.md)
- [AQS实现原理？](./docs/Lock/AQS.md)
- [Synchronized是如何从轻量级锁到重量级锁？](./docs/Lock/Synchronized.md)

## 消息中间件

### [消息中间件](./docs/MessageMiddleware)

- [为什么使用消息队列？消息队列有什么优点和缺点？Kafka、ActiveMQ、RabbitMQ、RocketMQ 都有什么优点和缺点？](./docs/MessageMiddleware/high-availability.md)
- [如何保证消息队列的高可用？消息丢失？](./docs/MessageMiddleware/message-middleware.md)
- [如何保证消息不被重复消费？（如何保证消息消费的幂等性）](./docs/MessageMiddleware/repeated-consumption.md)
- [如何保证消息的可靠性传输？（如何处理消息丢失的问题）](./docs/MessageMiddleware/reliable-transmission.md)
- [如何保证消息的顺序性？](./docs/MessageMiddleware/Sequence.md)
- [如何解决消息队列的延时以及过期失效问题？消息队列满了以后该怎么处理？有几百万消息持续积压几小时，说说怎么解决？](./docs/MessageMiddleware/message-backlog.md)
- [如果让你写一个消息队列，该如何进行架构设计啊？说一下你的思路。](./docs/MessageMiddleware/architecture-design.md)

## 搜索引擎

### [搜索引擎](./docs/ES)

- [ES 的分布式架构原理能说一下么（ES 是如何实现分布式的啊）？](./docs/ES/Principles-distributed-architecture.md)
- [ES 写入数据的工作原理是什么啊？ES 查询数据的工作原理是什么啊？底层的 Lucene 介绍一下呗？倒排索引了解吗？](./docs/ES/working-principle.md)
- [ES 在数据量很大的情况下（数十亿级别）如何提高查询效率啊？](./docs/ES/increase-efficiency.md)
- [ES 生产集群的部署架构是什么？每个索引的数据量大概有多少？每个索引大概有多少个分片？](./docs/ES/Indexes.md)

## 海量数据处理

### [海量数据处理](./docs/MassiveDataProcessing)

- [如何从大量的 URL 中找出相同的URL？](./docs/MassiveDataProcessing/URL.md)
- [如何从大量数据中找出高频词？](./docs/MassiveDataProcessing/high-frequency-words.md)
- [如何找出某一天访问百度网站最多的IP？](./docs/MassiveDataProcessing/IP.md)
- [如何在大量的数据中找出不重复的整数？](./docs/MassiveDataProcessing/no-repetition.md)
- [如何在大量的数据中判断一个数是否存在？](./docs/MassiveDataProcessing/existence.md)
- [如何查询最热门的查询串？](./docs/MassiveDataProcessing/most-popular.md)
- [如何统计不同电话号码的个数？](./docs/MassiveDataProcessing/statistical-quantity.md)
- [如何从 5 亿个数中找出中位数？](./docs/MassiveDataProcessing/median.md)
- [如何按照 query 的频度排序？](./docs/MassiveDataProcessing/sort.md)
- [如何找出排名前 500 的数？](./docs/MassiveDataProcessing/ranking.md)

## 解决问题的能力

### [解决问题的能力](./docs/ProblemSolvingSkills)

- [Linux常用命令，生产环境服务器变慢诊断，线上排查，性能评估？](./docs/ProblemSolvingSkills/linux.md)
- [如何快速定位一个项目跑的慢的原因在哪？](./docs/ProblemSolvingSkills/fast-positioning.md)

## 网络

### [网络](./docs/Network)

- [http，tcp，https，udp，特点与区别，TCP三次握手，四次挥手，OSI七层模型，TCP/IP四层模型，负载均衡算法？](./docs/Network/network-protocol.md)

## Netty

### [Netty](./docs/Netty)

- [ bio，nio ，aio](./docs/Netty/bio-nio-aio.md)
- [ 零拷贝？](./docs/Netty/zero-copy.md)
- [ 核心组件？](./docs/Netty/core-components.md)
- [ I/O 多路复用（epoll）？](./docs/Netty/epoll.md)
- [ netty架构设计怎么样子的？](./docs/Netty/architecture-design.md)

## 功能模块

### [功能模块](./docs/FunctionalModule)

- [幂等性实现](./docs/FunctionalModule/ImplementationIdempotency.md)
- [单点登录](./docs/FunctionalModule/single-sign.md)
- [金额篡改问题](./docs/FunctionalModule/amount-tampering.md)
- [秒杀场景设计](./docs/FunctionalModule/second-kill-scene-design.md)
- [库存超卖问题](./docs/FunctionalModule/inventory-oversold-problem.md)

## 日志

### [日志](./docs/Log)

- [log4j日志级别](./docs/Log/logLevel.md)

## 高并发

### [高并发](./docs/HighConcurrency)

- [高并发的实现](./docs/HighConcurrency/High-concurrency-implementation.md)